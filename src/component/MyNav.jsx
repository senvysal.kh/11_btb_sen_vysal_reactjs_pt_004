import React from "react";
import { NavLink } from "react-router-dom";
import { Container,Navbar,Nav } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';

export default function MyNav() {
  return (
    <div>
      <Navbar fixed="top" bg="light" expand="lg" className="position">
        <Container>
          <Navbar.Brand href="">Practice-004</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="justify-content-end" style={{ width: "100%" }}>
              <Nav.Link as={NavLink} to="/">
                Home
              </Nav.Link>
              <Nav.Link as={NavLink} to="/About">
                About
              </Nav.Link>
              <Nav.Link as={NavLink} to="/OurVision">
                OurVision
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  );
}
