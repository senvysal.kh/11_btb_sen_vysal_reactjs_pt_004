import { Routes,Route,} from "react-router-dom";
import Homepage from "./component/HomePage.";
import About from "./component/About";
import OurVision from "./component/OurVision";
import MyNav from "./component/MyNav";
import Footer from "./component/Footer";

function App () {
  return (
    <div className="App">
      <MyNav />
      <Routes>
        <Route path="/" element={<Homepage />} />
        <Route path="/About" element={<About />} />
        <Route path="/OurVision" element={<OurVision />} />
      </Routes>
      <Footer/>
    </div>
  ); 
};
export default App;
